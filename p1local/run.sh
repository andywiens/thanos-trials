#!/bin/bash

MYDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

docker run -v prom1data:/prometheus -v $MYDIR:/etc/prometheus -p 19091:9090 prom/prometheus
