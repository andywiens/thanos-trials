#!/bin/bash

MYDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

docker run -v prom0data:/prometheus -v $MYDIR:/etc/prometheus -p 19090:9090 prom/prometheus
